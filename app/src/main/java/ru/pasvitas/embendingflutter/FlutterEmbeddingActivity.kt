package ru.pasvitas.embendingflutter

import io.flutter.embedding.engine.FlutterEngine
import io.flutter.view.FlutterMain
import android.content.Context
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.android.FlutterFragment

class FlutterEmbeddingActivity : FlutterActivity(), FlutterFragment.FlutterEngineProvider {


    // You need to define an IntentBuilder subclass so that the
    // IntentBuilder uses FlutterEmbeddingActivity instead of a regular FlutterActivity.
    private class IntentBuilder// Override the constructor to specify your class.
    internal constructor() : FlutterActivity.IntentBuilder(FlutterEmbeddingActivity::class.java)

    // This is the method where you provide your existing FlutterEngine instance.

    override fun getFlutterEngine(context: Context): FlutterEngine? {
        init(context)
        return cachedflutterEngine
    }

    /*
    Интересная шняга получилось - где-то застрял флаттерский сдк и не хотел имплементить метод выше.
    В итоге зашел метод ниже...
    override fun provideFlutterEngine(context: Context): FlutterEngine? {
        return cachedflutterEngine
    }*/

    companion object {
        private lateinit var cachedflutterEngine: FlutterEngine

        fun init(context: Context) {
            if (::cachedflutterEngine.isInitialized) {
                return
            }
            // Flutter must be initialized before FlutterEngines can be created.
            FlutterMain.startInitialization(context)
            FlutterMain.ensureInitializationComplete(context, arrayOf())
            // Instantiate a FlutterEngine.
            cachedflutterEngine = FlutterEngine(context)

        }

        // This is the method that others will use to create
        // an Intent that launches MyFlutterActivity.
        fun createBuilder(): FlutterActivity.IntentBuilder {
            return IntentBuilder()
        }
    }

}